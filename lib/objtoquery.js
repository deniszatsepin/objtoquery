var _hasOwnProperty = function (obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
};

var pushToElements = function (elements, name, param, encode) {
  var pvalue = null;
  if (typeof param === 'string' || typeof param === 'number') {
    if (encode) {
      pvalue = encodeURIComponent(param);
    } else {
      pvalue = param;
    }
    elements.push(name + "=" + pvalue);
  }
};

/**
 *  This function converts object's params to query string.	
 *  @param {Object} obj - The request parameters for converting in quiery string 
 *  @param {Boolean} notUriEncode - Prevents request params from 'encodeUriComponent' encoding
 */
var objToQuery = exports.objToQuery = function (obj, notUriEncode) {
  if (typeof obj !== 'object') {
    throw new Error('Object to encode is not defined');
  }

  var encode = notUriEncode === true ? false : true;

  var elements = [];
  for (var propName in obj) {
    if (!_hasOwnProperty(obj, propName)) {
      continue;
    }
    var propValue = obj[propName], pval = null;
    if (propValue instanceof Array) {
      for (var i = 0, len = propValue.length; i < len; i += 1) {
        pushToElements(elements, propName, propValue[i], encode);
      }
    } else {
      pushToElements(elements, propName, propValue, encode);
    }
  }
  return elements.join('&');
};

/**
 *  Converts the request object to the query string and adds it to the url
 *  @param {Object} request - The request parameters
 *  @param {String} url - The url to witch you want to add parameters
 */
var addToUrl = exports.addToUrl = function(request, url) {
  if (typeof request !== 'object') {
    throw new Error('Request is not defined');
  }

  var result = '';
  url = url || '';
  var search = url.indexOf('?');
  var fragment = url.indexOf('#');

  var query = objToQuery(request);

  if (fragment !== -1 && search > fragment) {
    search = -1;
  }

  if (search === -1 && fragment === -1) {
    result = '?' + query;
  } else if (search === -1 && fragment !== -1) {
    result = url.substr(0, fragment) + '?' + query + url.substr(fragment);
  } else if (search !== -1 && fragment === -1) {
    result = url + '&' + query;
  } else {
    result = url.substr(0, fragment) + '&' + query + url.substr(fragment);
  }

  return result;
};
