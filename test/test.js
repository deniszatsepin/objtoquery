var should = require('chai').should();
var otq = require('../lib/objtoquery.js');
var objToQuery = otq.objToQuery;
var addToUrl = otq.addToUrl;

describe('objtoquery', function () {

  describe('objToQuery', function() {

    it('should return error', function () {
      objToQuery.should.throw(/Object to encode is not defined/);
    });

    it('should return null string for {}', function () {
      var query = objToQuery({});
      query.should.equal('');
    });

    it('should return null string for {null, function, {}}', function () {
      var object = {
        param1: null,
        param2: {name: ''},
        param3: function(){return null;}
      };
      var query = objToQuery(object);
      query.should.equal('');
    });

    it('should return correct query string for \'correct\' object', function () {
      var object = {
        param1: 1,
        param2: '2',
        param3: [1, '2']
      };
      var query = objToQuery(object);
      query.should.equal('param1=1&param2=2&param3=1&param3=2');
    });

    it('should return correct query string for \'incorrect\' object', function () {
      var object = {
        param1: 1,
        param2: '2',
        param3: [1, '2',{a:'1'}],
        param4: function(){}
      };
      var query = objToQuery(object);
      query.should.equal('param1=1&param2=2&param3=1&param3=2');
    });

    it('should return correct encoded query string', function () {
      var person = {
        name: 'Denis Zatsepin',
        email: 'denis@zatsepin.spb.ru'
      };
      var query = objToQuery(person);
      query.should.equal('name=Denis%20Zatsepin&email=denis%40zatsepin.spb.ru');
    });
  });

  describe('addToUrl', function () {
    var request = {
      name: 'Denis',
      age: 20
    };

    it('should return error', function () {
      addToUrl.should.throw(/Request is not defined/);
    });

    it('should return correct url', function () {
      addToUrl(request, '').should.equal('?name=Denis&age=20');
    });

    it('should return correct url (search defined)', function () {
      addToUrl(request, 'www.yandex.ru/a/b/?a=b').should.equal('www.yandex.ru/a/b/?a=b&name=Denis&age=20');
    });

    it('should return correct url (search and fragment defined)', function () {
      addToUrl(request, 'www.yandex.ru/a/b/?a=b#last').should.equal('www.yandex.ru/a/b/?a=b&name=Denis&age=20#last');
    });

    it('should return correct url (fragment defined)', function () {
      addToUrl(request, 'www.yandex.ru/#last').should.equal('www.yandex.ru/?name=Denis&age=20#last');
    });

    it('should return correct url (search after fragment)', function () {
      addToUrl(request, 'www.yandex.ru/#last?a').should.equal('www.yandex.ru/?name=Denis&age=20#last?a');
    });

  });	
});
